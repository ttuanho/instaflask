# Instaflask
A clone of Instagram using Flask.

Work in progress.

## TODO
- [x] Ability to create, read, update, and delete posts through the API.
- [x] Show list of posts in the index page.
- [x] Ability to upload posts though a form.
- [x] Single pages for posts, with links to edit and remove them.
- [x] Pagination.
- [x] Use React Router.
- [ ] Use Redux.
- [ ] Add full user management with authentication/authorization system.
- [ ] Deploy to Heroku.

The usage of React Router and Redux (and even React) may be overkill for this project at the current stage (and maybe at the final stage too), but it doesn't matter since this is a *learning project*.
