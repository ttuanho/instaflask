import axios from 'axios'

export const REQUEST_POSTS = 'REQUEST_POSTS'
function requestPosts() {
  return {
    type: REQUEST_POSTS
  }
}

export const RECEIVE_POSTS = 'RECEIVE_POSTS'
function receivePosts(json) {
  return {
    type: RECEIVE_POSTS,
    posts: json.data.posts
  }
}

export const fetchPosts = () => {
  return (dispatch) => {
    dispatch(requestPosts())
    return axios.get('/api/posts')
      .then((response) => dispatch(receivePosts(response)))
  }
}
