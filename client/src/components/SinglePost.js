import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { CSSTransitionGroup } from 'react-transition-group'
import axios from 'axios'

import Post from './Post'

class SinglePost extends Component {
  constructor(props) {
    super(props)
    this.state = {
      post: {},
      redirect: false,
      description: '',
      editing: false,
    }
  }

  componentDidMount() {
    this.fetchPost(this.props.match.params.id)
  }

  handleChange = (e) => {
    this.setState({ description: e.target.value })
  }

  handleSubmit = (e) => {
    e.preventDefault()
    const description = this.state.description.trim()
    const id = this.state.post.id

    if (description && id) {
      axios.patch(`/api/posts/${id}`, { description })
           .then((response) => {
             this.fetchPost(id)
             this.setState({ editing: false })
           })
           .catch((error) => {
             console.log(error)
           })
    }
  }

  fetchPost = (id) => {
    axios.get(`/api/posts/${id}`)
         .then((response) => {
           const data = response.data
           this.setState({ post: data, description: data.description })
         })
         .catch((error) => {
           console.log(error)
         })
  }

  deletePost = (id) => {
    axios.delete(`/api/posts/${id}`)
         .then((response) => {
           this.setState({ redirect: true })
         })
         .catch((error) => {
           console.log(error)
         })
  }

  render() {
    const { redirect, editing } = this.state
    let editForm = null

    if (redirect) {
      return <Redirect to="/" />
    }

    if (editing) {
      editForm = (
        <form onSubmit={this.handleSubmit}>
          <div className="field">
            <div className="control">
              <textarea
                className="textarea"
                placeholder="description"
                value={this.state.description}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="field">
            <div className="control">
              <input
                type="submit"
                className="button is-success is-outlined"
                value="Update"
              />
            </div>
          </div>
        </form>
      )
    }

    return (
      <div className="section">
        <div className="columns is-mobile is-centered">
          <div className="column is-5-desktop">
            <Post {...this.state.post} />
            <div className="field is-grouped">
              <p className="control">
                <a
                  className="button is-primary is-outlined"
                  onClick={() => {this.setState((prevState) => (
                    { editing: !prevState.editing }
                  ))}}
                >
                  <span className="icon">
                    <i className="ion-edit"></i>
                  </span>
                  <span>Edit</span>
                </a>
              </p>
              <p className="control">
                <a
                  className="button is-danger is-outlined"
                  onClick={() => {this.deletePost(this.state.post.id)}}
                >
                  <span className="icon">
                    <i className="ion-close"></i>
                  </span>
                  <span>Remove</span>
                </a>
              </p>
            </div>
            <CSSTransitionGroup
              transitionName="edit"
              transitionEnterTimeout={300}
              transitionLeaveTimeout={300}
            >
              {editForm}
            </CSSTransitionGroup>
          </div>
        </div>
      </div>
    )
  }
}

export default SinglePost
