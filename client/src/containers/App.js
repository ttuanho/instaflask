import React from 'react'
import { Route, Switch } from 'react-router-dom'

import Header from '../components/Header'
import PostList from './PostList'
import SinglePost from '../components/SinglePost'

const App = () => (
  <div>
    <Header />
    <Switch>
      <Route exact path="/" component={PostList} />
      <Route path="/page/:page" component={PostList} />
      <Route path="/post/:id" component={SinglePost} />
    </Switch>
  </div>
)

export default App
