import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import Post from '../components/Post'
import { fetchPosts } from '../actions'

class PostList extends Component {
  componentDidMount() {
    this.props.dispatch(fetchPosts())
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location !== this.props.location) {
      this.setState({ page: nextProps.match.params.page }, () => {
        this.props.dispatch(fetchPosts())
      })
    }
  }

  render() {
    return (
      <div className="section">
        <div className="columns is-mobile is-centered">
          <div className="column is-5-desktop">
            {this.props.isFetching
             ? <div className="has-loader"></div>
             : this.props.posts.map((post) => (
               <Post key={post.id} {...post} />
             ))}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { isFetching, posts } = state
  return {
    isFetching,
    posts
  }
}

export default withRouter(connect(mapStateToProps)(PostList))
