import os

class BaseConfig(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'secret'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///app.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    ERROR_404_HELP = False      # used on 404 abort response in Flask-RESTful
    POSTS_PER_PAGE = 5

class DevelopmentConfig(BaseConfig):
    DEBUG = True
    POSTS_PER_PAGE = 2

class TestingConfig(BaseConfig):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://' # implicit :memory:
