"""Tests for the Instaflask server."""

import pytest

from server.app import create_app
from server.config import TestingConfig
from server.models import db, Post


@pytest.fixture
def app():
    """Return a Flask application configured for testing."""
    app = create_app(TestingConfig)

    with app.app_context():
        db.create_all()
        yield app

        # Tear-down.
        db.session.remove()
        db.drop_all()


@pytest.fixture
def client(app):
    """Return a test client from the application returned by app()."""
    client = app.test_client()
    return client


def test_empty_database(client):
    response = client.get('/api/posts')
    assert response.data == b'[]\n'


def test_non_empty_database(client):
    db.session.add(Post(description='test'))
    db.session.commit()
    response = client.get('/api/posts')
    assert response.data != b'[]\n'
